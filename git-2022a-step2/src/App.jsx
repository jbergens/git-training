import React from 'react';
import { DemoGraph } from './demo-graph';

const App = () => {
  return (
    <div className="p-6 bg-indigo-200">
      <div className="">
          <h1>How to use Git</h1>
          <p className="bg-orange-300">Will be written later
          </p>
      </div>
      <div>
        <DemoGraph />
      </div>
    </div>
  );
};

export default App;
